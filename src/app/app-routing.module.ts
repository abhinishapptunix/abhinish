import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdduserComponent } from './adduser/adduser.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [{path:'',loadChildren:()=>import('./list/list.module').then((m)=>m.ListModule)},{path:'',loadChildren:()=>import('./adduser/adduser.module').then((m)=>m.AdduserModule)},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
