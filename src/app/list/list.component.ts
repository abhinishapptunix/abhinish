import { getLocaleCurrencyCode } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { timeStamp } from 'console';
import { observable } from 'rxjs';
import { ServiceService } from '../service.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { AddUserComponent } from '../modal/add-user/add-user.component';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
  displayedColumns: string[] = [ 'employee age','employee name', 'employee salary','Action'];
  dataSource:any;
  pageSize: number= 10;
  currentPage: number=1;
  recordsLength: number=1;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort) sort: MatSort | undefined;
  

  constructor(private service:ServiceService,private http: HttpClient,
    private router:ActivatedRoute) {
  this.dataSource = ELEMENT_DATA;
  
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    var x = "Abhinish";
    var y = "khan";
    this.getData();
    // this.update();
    // this.deletePost()
    
  }
    deletePost(id:number) {
         this.service.delete( "delete",id
      ).subscribe(data => {
        console.log(data);
        this.getData();
      });
    }
      // update() {
      //   this.service.update('update', this.router.snapshot.params['id'], this.dataSource.value)
      //   .subscribe((data:any) => {
      //     console.warn(data);
      //   })
      // }
  

      getData(){
      this.service.getAPI('getAll').subscribe((data:any)=>{
        console.log(data);
        if(data){ 
          this.dataSource=data['allEmployees'];
        }
      })
    }
  }