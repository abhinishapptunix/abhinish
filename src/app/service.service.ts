import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  hideSpinner() {
    throw new Error("Method not implemented.");
  }
  errorMsg(arg0: string) {
    throw new Error("Method not implemented.");
  }
  base_url="http://www.appgrowthcompany.com:5069/api/v1/employee/";
  constructor(private http: HttpClient) { }
  getAPI(url:string){
    return this.http.get(this.base_url+url);
  }
  delete(url:string,id:any){
    console.log(this.http.delete(this.base_url+url));
    
    return this.http.delete(this.base_url+url+'/'+id);
  }
  savelist(url:string,data:any){
    return this.http.post(this.base_url+url,data);
  }
  getById(url:string,url1:string){
    console.log(url,url1);
    console.log(this.base_url+url+'/'+url1);
  
    return this.http.get(this.base_url+url+'/'+url1);
  }
  update(url:string,id:any,data:any){
    return this.http.put(this.base_url+url+'/'+id,data); 
  } 
  
}