import { Component, OnInit } from '@angular/core';
import { Validators,FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.scss']
})
export class AdduserComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean= false;
  id: any;
  constructor(private formBuilder:FormBuilder, private router: Router,
    private service:ServiceService,private route:ActivatedRoute ) {
    this.loginForm = this.formBuilder.group ({
      employee_age:['',[Validators.required]],
      employee_name:['',[Validators.required]],
      employee_salary:['',[Validators.required]],
    })
  }
  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    if(this.id){
      this.getById()
    }
    console.log(this.id);
  }
  getById(){
  this.service.getById('get',this.id).subscribe((res:any)=>{
    if(res){
      this.loginForm.patchValue({ 
        employee_age:res.employee_age,
        employee_name:res.employee_name,
        employee_salary:res.employee_salary

      });
      console.log(res); 
    }
  })

  }
  onSubmit(){
    if (this.loginForm.valid) {
    console.log(this.loginForm.value);
      if(!this.id){
        this.service.savelist('create',this.loginForm.value).subscribe(res=>{
          if(res){
            this.router.navigate(['']);

          }

              })
      }
      else{
        this.service.update('/update',this.id,this.loginForm.value).subscribe(res=>{
          if(res){
            this.router.navigate(['']);
          }
        })
      }
  }
    
  } 
}
