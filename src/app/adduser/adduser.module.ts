import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdduserRoutingModule } from './adduser-routing.module';
import { AdduserComponent } from './adduser.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [AdduserComponent],
  imports: [
    CommonModule,FormsModule,ReactiveFormsModule,
    AdduserRoutingModule
  ]
})
export class AdduserModule { }
