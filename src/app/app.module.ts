import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdduserComponent } from './adduser/adduser.component';
import { ListComponent } from './list/list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceService } from './service.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { DataComponent } from './data/data.component';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { GetInterceptorService } from './get-interceptor/get-interceptor.service';

import {MatButtonModule} from '@angular/material/button';
import { AddUserComponent } from './modal/add-user/add-user.component';

@NgModule({
  declarations: [
    AddUserComponent,
    AppComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    FormsModule,
    ReactiveFormsModule,
    FilterPipeModule,
    MatButtonModule,
    
    
  ],
  providers: [{
    provide:HTTP_INTERCEPTORS,
    useClass:GetInterceptorService,multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
